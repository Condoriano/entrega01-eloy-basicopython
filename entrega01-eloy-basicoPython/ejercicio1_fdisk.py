

#binevenida
print(
 """\n\n\n\n\n\n\nWelcome to fdisk (util-linux 2.27.1).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.""")
#variables

partitions=[["1",True,"Linux"],["2",True],["3",False],["4",False],["5",True]]
dosComp=True
display=True
#part = dict()
#part[1] = True
#part[2] = True
#part[3] = False
#part[4] = False
#part [5] =True

#funciones


#m:mostrar ayuda
def print_help():
	print(
"""\n\nHelp:

  Sun
   a   toggle the read-only flag
   c   toggle the mountable flag

  Generic
   d   delete a partition
   F   list free unpartitioned space
   l   list known partition types
   n   add a new partition
   p   print the partition table
   t   change a partition type
   v   verify the partition table
   i   print information about a partition

  Misc
   m   print this menu
   u   change display/entry units
   x   extra functionality (experts only)

  Script
   I   load disk layout from sfdisk script file
   O   dump disk layout to sfdisk script file

  Save & Exit
   w   write table to disk and exit
   q   quit without saving changes

  Create a new label
   g   create a new empty GPT partition table
   G   create a new empty SGI (IRIX) partition table
   o   create a new empty DOS partition table
   s   create a new empty Sun partition table
\n
""")


#a    COMPLETO
def toogle_bootable_flag():
	bucle=True
	while bucle==True:
		tecla2=str(input("Número de partición"+ partitionList))
		for px in partitions:
			if tecla2 in px[0]:
				px[1]=not px[1]
				if px[1]==True:
					enDis="enabled"
				else:
					enDis="disabled"
				print("The bootable flag on partition "+ px[0] + " is " + enDis + " now")
				bucle=not bucle
				break
		else:
			print("Value out of range") #color rojo
			break

#b	COMPLETO
def edit_nested_BSD():
	print("The device (null) does not contain BSD disklabel.")
	tecla2=str(input("Do you want to create a BSD disklabel? [Y]es/[N]o:"))
	if (tecla2=="y") or (tecla2=="Y"):
		print("There is no *BSD partition on /dev/sda.")
	elif (tecla2=="n")or (tecla2=="N"):
		pass
#c	COMPLETO
def c():
	global dosComp
	dosComp = not dosComp
	if dosComp==True:
		print("DOS Compatibility flag is set (DEPRECATED!)")
	else:
		print("DOS Compatibility flag is not set")

#d	INCOMPLETO
def delete_partition():

	while bucle==True:
		tecla2=str(input("Numero de partición " + (partitionList)+ ":"))
		if tecla2 in partitionList:
			#partitions.remove (tecla2)
			break
		else:
			print ("Value out of range.")


#F	COMPLETO
def list_free_unpartition():
	print(
"""\nUnpartitioned space /dev/sda: 0 B, 0 bytes, 0 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
""")

#l	COMPLETO
def list_know_partition_tipes():
	print(
"""\n0  Vacía           24  DOS de NEC      81  Minix / Linux a bf  Solaris
 1  FAT12           27  WinRE NTFS ocul 82  Linux swap / So c1  DRDOS/sec (FAT-
 2  XENIX root      39  Plan 9          83  Linux           c4  DRDOS/sec (FAT-
 3  XENIX usr       3c  PartitionMagic  84  OS/2 hidden or  c6  DRDOS/sec (FAT-
 4  FAT16 <32M      40  Venix 80286     85  Linux extendida c7  Syrinx
 5  Extendida       41  PPC PReP Boot   86  Conjunto de vol da  Datos sin SF
 6  FAT16           42  SFS             87  Conjunto de vol db  CP/M / CTOS / .
 7  HPFS/NTFS/exFAT 4d  QNX4.x          88  Linux plaintext de  Utilidad Dell
 8  AIX             4e  QNX4.x segunda  8e  Linux LVM       df  BootIt
 9  AIX arrancable  4f  QNX4.x tercera  93  Amoeba          e1  DOS access
 a  Gestor de arran 50  OnTrack DM      94  Amoeba BBT      e3  DOS R/O
 b  W95 FAT32       51  OnTrack DM6 Aux 9f  BSD/OS          e4  SpeedStor
 c  W95 FAT32 (LBA) 52  CP/M            a0  Hibernación de  ea  Rufus alignment
 e  W95 FAT16 (LBA) 53  OnTrack DM6 Aux a5  FreeBSD         eb  BeOS fs
 f  W95 Ext'd (LBA) 54  OnTrackDM6      a6  OpenBSD         ee  GPT
10  OPUS            55  EZ-Drive        a7  NeXTSTEP        ef  EFI (FAT-12/16/
11  FAT12 oculta    56  Golden Bow      a8  UFS de Darwin   f0  inicio Linux/PA
12  Compaq diagnost 5c  Priam Edisk     a9  NetBSD          f1  SpeedStor
14  FAT16 oculta <3 61  SpeedStor       ab  arranque de Dar f4  SpeedStor
16  FAT16 oculta    63  GNU HURD o SysV af  HFS / HFS+      f2  DOS secondary
17  HPFS/NTFS ocult 64  Novell Netware  b7  BSDI fs         fb  VMFS de VMware
18  SmartSleep de A 65  Novell Netware  b8  BSDI swap       fc  VMKCORE de VMwa
1b  FAT32 de W95 oc 70  DiskSecure Mult bb  Boot Wizard hid fd  Linux raid auto
1c  FAT32 de W95 (L 75  PC/IX           bc  Acronis FAT32 L fe  LANstep
1e  FAT16 de W95 (L 80  Minix antiguo   be  arranque de Sol ff  BBT
""")

#n	COMPLETO(mejoras posibles: crear una particion si existe un espacio libre, listar particiones reales )
def n():
	tecla2=str(input("""Número de partición (4-8, default 4)"""))
	if tecla2 == "":
		print("\nOther partitions already cover the whole disk. Delete some/shrink them before retry.")
	elif (int(tecla2) >= 4) and (int(tecla2) <= 8):
		print ("\nOther partitions already cover the whole disk. Delete some/shrink them before retry.")
	elif (str(tecla2) != "") :
		print("Value out of range")

#p	COMPLETO
def p():
	print(
"""\nDisk /dev/sda: 10 GiB, 10737418240 bytes, 20971520 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x39061628

Disposit.  Inicio    Start    Final Sectores  Size Id Tipo
/dev/sda1  *          2048 18874367 18872320    9G 83 Linux
/dev/sda2         18876414 20969471  2093058 1022M  5 Extendida
/dev/sda5         18876416 20969471  2093056 1022M 82 Linux swap / Solaris
""")




def t(): #COMPLETO (Sin lista)
	bucle=True
	bucle2=True
	while bucle==True:
		tecla2=str(input("Número de partición"+partitionList))
		if str(tecla2) in partitionList:
			while bucle2==True:
				tecla3=input("Partition type (type L to list all types):")
				if (tecla3=="l") or (tecla3=="L"):
					print(
""" 0  Vacía           24  DOS de NEC      81  Minix / Linux a bf  Solaris
 1  FAT12           27  WinRE NTFS ocul 82  Linux swap / So c1  DRDOS/sec (FAT-
 2  XENIX root      39  Plan 9          83  Linux           c4  DRDOS/sec (FAT-
 3  XENIX usr       3c  PartitionMagic  84  OS/2 hidden or  c6  DRDOS/sec (FAT-
 4  FAT16 <32M      40  Venix 80286     85  Linux extendida c7  Syrinx
 5  Extendida       41  PPC PReP Boot   86  Conjunto de vol da  Datos sin SF
 6  FAT16           42  SFS             87  Conjunto de vol db  CP/M / CTOS / .
 7  HPFS/NTFS/exFAT 4d  QNX4.x          88  Linux plaintext de  Utilidad Dell
 8  AIX             4e  QNX4.x segunda  8e  Linux LVM       df  BootIt
 9  AIX arrancable  4f  QNX4.x tercera  93  Amoeba          e1  DOS access
 a  Gestor de arran 50  OnTrack DM      94  Amoeba BBT      e3  DOS R/O
 b  W95 FAT32       51  OnTrack DM6 Aux 9f  BSD/OS          e4  SpeedStor
 c  W95 FAT32 (LBA) 52  CP/M            a0  Hibernación de  ea  Rufus alignment
 e  W95 FAT16 (LBA) 53  OnTrack DM6 Aux a5  FreeBSD         eb  BeOS fs
 f  W95 Ext'd (LBA) 54  OnTrackDM6      a6  OpenBSD         ee  GPT
10  OPUS            55  EZ-Drive        a7  NeXTSTEP        ef  EFI (FAT-12/16/
11  FAT12 oculta    56  Golden Bow      a8  UFS de Darwin   f0  inicio Linux/PA
12  Compaq diagnost 5c  Priam Edisk     a9  NetBSD          f1  SpeedStor
14  FAT16 oculta <3 61  SpeedStor       ab  arranque de Dar f4  SpeedStor
16  FAT16 oculta    63  GNU HURD o SysV af  HFS / HFS+      f2  DOS secondary
17  HPFS/NTFS ocult 64  Novell Netware  b7  BSDI fs         fb  VMFS de VMware
18  SmartSleep de A 65  Novell Netware  b8  BSDI swap       fc  VMKCORE de VMwa
1b  FAT32 de W95 oc 70  DiskSecure Mult bb  Boot Wizard hid fd  Linux raid auto
1c  FAT32 de W95 (L 75  PC/IX           bc  Acronis FAT32 L fe  LANstep
1e  FAT16 de W95 (L 80  Minix antiguo   be  arranque de Sol ff  BBT
""")
				elif tecla3=="1":
					print("Changed type of partition from " + " to FAT12") #tipo de particion
					bucle=False
					break
				else:
					print("Value out of range")
					pass
			else:
					print("Value out of range")
					pass

#v	COMPLETO
def v():
	print("""Remaining 4095 unallocated 512-byte sectors.""")

#i	COMPLETO

def i():
	tecla2=input("Número de partición "+(partitionList))
	print("""\n Device: /dev/sda1
           Boot: *
          Start: 2048
            End: 18874367
        Sectors: 18872320
      Cylinders: 1175
           Size: 9G
             Id: 1
           Type: FAT12
    Start-C/H/S: 0/33/32
      End-C/H/S: 1023/63/254
          Attrs: 80
""")

#u	COMPLETO
def u():
	global display
	display = not display
	if display==True:
		print("Changing display/entry units to cylinders (DEPRECATED!).")
	else:
		print("Changing display/entry units to sectors.")


#x	COMPLETO
def x():
	while bucle==True:
		tecla2=(input("Orden avanzada (m para obtener ayuda):"))
		if tecla2=="q": #COMPLETO
			exit()
		elif tecla2=="b":
			xb()	#Casi Completo
		elif tecla2=="i":
			xi()
		elif tecla2=="c":
			xc()
		elif tecla2=="h":
			xh()
		elif tecla2=="s":
	                xs()
		elif tecla2=="p":
	                xp()
		elif tecla2=="v":
	                xv()
		elif tecla2=="d":
	                xd()
		elif tecla2=="D":
	                xD()
		elif tecla2=="f":
	                xf()
		elif tecla2=="m":
	                xm()
		elif tecla2=="r": #COMPLETO
			break


def xb():	#Casi COMPLETO(falta distinguir si teclaAv2 es un numero valido o no)
	teclaAv=input("Número de partición"+partitionList)
	if teclaAv in partitionList:
		teclaAv2=input("Nuevo principio de datos (1-18874367, default 2048):")
		print("\n")
	else:
		print("Value out of range")

def xi():	#INCOMPLETO (Igual al de arriba)
	teclaAv=input("Enter the new disk identifier:")
	print("Disk identifier changed from " + " to")

def xc():	#INCOMPLETO (Igual al de arriba)
	while bucle==True:
		teclaAv=input("Número de cilindros (1-1048576, default 1305):")
		print("\n")
		break

def xh():	#INCOMPLETO(de nuevo, falta la excepción)
	while bucle==True:
		teclaAv=input("Número de cabezas (1-256, default 1):")
		print ("\n")
		break
def xs():	#INCOMPLETO (copy/paste del de arriba)
	while bucle==True:
		teclaAv=input("Número de sectores (1-63, default 63):")
		print("\n")
		break
#Xp	COMPLETO
def xp():
	print(
"""\nDisk /dev/sda: 10 GiB, 10737418240 bytes, 20971520 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x39061628

Disposit.  Inicio    Start    Final Sectores Id Tipo                 Start-C/H/S   End-C/H/S Attrs
/dev/sda1  *          2048 18874367 18872320 83 Linux                    0/33/32 1023/63/254    80
/dev/sda2         18876414 20969471  2093058  5 Extendida            1023/63/254 1023/63/254
/dev/sda5         18876416 20969471  2093056 82 Linux swap / Solaris 1023/63/254 1023/63/254
\n
""")
def xv():
	print("Remaining 4095 unallocated 512-byte sectors.")
#Xd	COMPLETO
def xd():
	print(
"""\nFirst sector: offset = 0, size = 512 bytes.
00000000  eb 63 90 10 8e d0 bc 00  b0 b8 00 00 8e d8 8e c0
00000010  fb be 00 7c bf 00 06 b9  00 02 f3 a4 ea 21 06 00
00000020  00 be be 07 38 04 75 0b  83 c6 10 81 fe fe 07 75
00000030  f3 eb 16 b4 02 b0 01 bb  00 7c b2 80 8a 74 01 8b
00000040  4c 02 cd 13 ea 00 7c 00  00 eb fe 00 00 00 00 00
00000050  00 00 00 00 00 00 00 00  00 00 00 80 01 00 00 00
00000060  00 00 00 00 ff fa 90 90  f6 c2 80 74 05 f6 c2 70
00000070  74 02 b2 80 ea 79 7c 00  00 31 c0 8e d8 8e d0 bc
00000080  00 20 fb a0 64 7c 3c ff  74 02 88 c2 52 bb 17 04
00000090  f6 07 03 74 06 be 88 7d  e8 17 01 be 05 7c b4 41
000000a0  bb aa 55 cd 13 5a 52 72  3d 81 fb 55 aa 75 37 83
000000b0  e1 01 74 32 31 c0 89 44  04 40 88 44 ff 89 44 02
000000c0  c7 04 10 00 66 8b 1e 5c  7c 66 89 5c 08 66 8b 1e
000000d0  60 7c 66 89 5c 0c c7 44  06 00 70 b4 42 cd 13 72
000000e0  05 bb 00 70 eb 76 b4 08  cd 13 73 0d 5a 84 d2 0f
000000f0  83 d0 00 be 93 7d e9 82  00 66 0f b6 c6 88 64 ff
00000100  40 66 89 44 04 0f b6 d1  c1 e2 02 88 e8 88 f4 40
00000110  89 44 08 0f b6 c2 c0 e8  02 66 89 04 66 a1 60 7c
00000120  66 09 c0 75 4e 66 a1 5c  7c 66 31 d2 66 f7 34 88
00000130  d1 31 d2 66 f7 74 04 3b  44 08 7d 37 fe c1 88 c5
00000140  30 c0 c1 e8 02 08 c1 88  d0 5a 88 c6 bb 00 70 8e
00000150  c3 31 db b8 01 02 cd 13  72 1e 8c c3 60 1e b9 00
00000160  01 8e db 31 f6 bf 00 80  8e c6 fc f3 a5 1f 61 ff
00000170  26 5a 7c be 8e 7d eb 03  be 9d 7d e8 34 00 be a2
00000180  7d e8 2e 00 cd 18 eb fe  47 52 55 42 20 00 47 65
00000190  6f 6d 00 48 61 72 64 20  44 69 73 6b 00 52 65 61
000001a0  64 00 20 45 72 72 6f 72  0d 0a 00 bb 01 00 b4 0e
000001b0  cd 10 ac 3c 00 75 f4 c3  28 16 06 39 00 00 80 20
000001c0  21 00 83 fe ff ff 00 08  00 00 00 f8 1f 01 00 fe
000001d0  ff ff 05 fe ff ff fe 07  20 01 02 f0 1f 00 00 00
000001e0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
000001f0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 55 aa\n
""")
#XD	COMPLETO
def xD():
	print(
"""\nMBR: offset = 0, size = 512 bytes.
00000000  eb 63 90 10 8e d0 bc 00  b0 b8 00 00 8e d8 8e c0
00000010  fb be 00 7c bf 00 06 b9  00 02 f3 a4 ea 21 06 00
00000020  00 be be 07 38 04 75 0b  83 c6 10 81 fe fe 07 75
00000030  f3 eb 16 b4 02 b0 01 bb  00 7c b2 80 8a 74 01 8b
00000040  4c 02 cd 13 ea 00 7c 00  00 eb fe 00 00 00 00 00
00000050  00 00 00 00 00 00 00 00  00 00 00 80 01 00 00 00
00000060  00 00 00 00 ff fa 90 90  f6 c2 80 74 05 f6 c2 70
00000070  74 02 b2 80 ea 79 7c 00  00 31 c0 8e d8 8e d0 bc
00000080  00 20 fb a0 64 7c 3c ff  74 02 88 c2 52 bb 17 04
00000090  f6 07 03 74 06 be 88 7d  e8 17 01 be 05 7c b4 41
000000a0  bb aa 55 cd 13 5a 52 72  3d 81 fb 55 aa 75 37 83
000000b0  e1 01 74 32 31 c0 89 44  04 40 88 44 ff 89 44 02
000000c0  c7 04 10 00 66 8b 1e 5c  7c 66 89 5c 08 66 8b 1e
000000d0  60 7c 66 89 5c 0c c7 44  06 00 70 b4 42 cd 13 72
000000e0  05 bb 00 70 eb 76 b4 08  cd 13 73 0d 5a 84 d2 0f
000000f0  83 d0 00 be 93 7d e9 82  00 66 0f b6 c6 88 64 ff
00000100  40 66 89 44 04 0f b6 d1  c1 e2 02 88 e8 88 f4 40
00000110  89 44 08 0f b6 c2 c0 e8  02 66 89 04 66 a1 60 7c
00000120  66 09 c0 75 4e 66 a1 5c  7c 66 31 d2 66 f7 34 88
00000130  d1 31 d2 66 f7 74 04 3b  44 08 7d 37 fe c1 88 c5
00000140  30 c0 c1 e8 02 08 c1 88  d0 5a 88 c6 bb 00 70 8e
00000150  c3 31 db b8 01 02 cd 13  72 1e 8c c3 60 1e b9 00
00000160  01 8e db 31 f6 bf 00 80  8e c6 fc f3 a5 1f 61 ff
00000170  26 5a 7c be 8e 7d eb 03  be 9d 7d e8 34 00 be a2
00000180  7d e8 2e 00 cd 18 eb fe  47 52 55 42 20 00 47 65
00000190  6f 6d 00 48 61 72 64 20  44 69 73 6b 00 52 65 61
000001a0  64 00 20 45 72 72 6f 72  0d 0a 00 bb 01 00 b4 0e
000001b0  cd 10 ac 3c 00 75 f4 c3  28 16 06 39 00 00 80 20
000001c0  21 00 83 fe ff ff 00 08  00 00 00 f8 1f 01 00 fe
000001d0  ff ff 05 fe ff ff fe 07  20 01 02 f0 1f 00 00 00
000001e0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
000001f0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 55 aa

EBR: offset = 9664723968, size = 512 bytes.
2400ffc00  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
*
2400ffdb0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 fe
2400ffdc0  ff ff 82 fe ff ff 02 00  00 00 00 f0 1f 00 00 00
2400ffdd0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00
*
2400ffdf0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 55 aa\n
""")
#XF	COMPLETO
def xf():
	print("Nothing to do. Ordering is correct already.")
#XM	COMPLETO
def xm():
	print(
"""\nHelp (expert commands):

  DOS (MBR)
   b   move beginning of data in a partition
   i   change the disk identifier

  Geometría
   c   change number of cylinders
   h   change number of heads
   s   change number of sectors/track

  Generic
   p   print the partition table
   v   verify the partition table
   d   print the raw data of the first sector from the device
   D   print the raw data of the disklabel from the device
   f   fix partitions order
   m   print this menu

  Save & Exit
   q   quit without saving changes
   r   return to main menu
""")
	x()
#I	COMPLETO(mejoras posibles)
def I():
	scriptName=input(str("Enter script file name:"))
	print("Cannot open : No existe el archivo o el directorio")

#O	COMPLETO(mejoras posibles)
def O():
	scriptName=input(str("Enter script file name:"))
	print("Cannot open : No existe el archivo o el directorio")
#w	COMPLETO
def w():
	print(
"""\nThe partition table has been altered.
Calling ioctl() to re-read partition table.
Re-reading the partition table failed.: Dispositivo o recurso ocupado

The kernel still uses the old table. The new table will be used at the next reboot orafter you run partprobe(8) or kpartx(8).
"""+"""\n\nesto es solo una prueba""")
#g	COMPLETO
def g():
	print("""Created a new GPT disklabel (GUID: E0E1EE7E-9B19-40D7-B0F0-CD68A8B6BE81).""")
#G	COMPLETO
def G():
	print(
"""\nCreated a new partition 11 of type 'SGI volume' and of size 10 GiB.
Created a new partition 9 of type 'SGI volhdr' and of size 2 MiB.
Created a new SGI disklabel.
""")
#o	COMPLETO
def o():
	print("Created a new DOS disklabel with disk identifier 0x3f755dea.")
#s	COMPLETO
def s():
	print(
"""\nCreated a new partition 1 of type 'Linux native' and of size 10 GiB.
Created a new partition 2 of type 'Linux swap' and of size 47,1 MiB.
Created a new partition 3 of type 'Whole disk' and of size 10 GiB.
Created a new Sun disklabel.
""")


#Lista de particiones

partitionList=""
bucle=True
for p in partitions:
	partitionList=(partitionList + p[0]+ ",")
partitionList="(" + partitionList + ")"

#MENU
while True:
	#pusla una tecla y enter para acceder al menu
	tecla=str(input("Orden (m para obtener ayuda):"))
	#Q:quit
	if tecla =="q":
		break
	#m:ayuda
	elif tecla =="m":
		print_help()

	#a
	elif tecla =="a":
		toogle_bootable_flag()

	#b
	elif tecla =="b":
		edit_nested_BSD()

	#c
	elif tecla =="c":
		c()

	#d
	elif tecla =="d":
		delete_partition()

	#F
	elif tecla=="F":
		list_free_unpartition()

	#l
	elif tecla=="l":
		list_know_partition_tipes()

	#n
	elif tecla=="n":
		n()
	#p
	elif tecla=="p":
		p()
	#t
	elif tecla=="t":
		t()
	#v
	elif tecla=="v":
		v()
	#i
	elif tecla=="i":
		i()



	#u
	elif tecla =="u":
		u()
	#x
	elif tecla =="x":
		x()
	#I
	elif tecla =="I":
		I()
	#O
	elif tecla =="O":
		O()



	#w
	elif tecla=="w":
		w()
	#g
	elif tecla=="g":
		g()
	#G
	elif tecla=="G":
		G()
	#o
	elif tecla=="o":
		o()

	#s
	elif tecla=="s":
		s()
